package com.example.homework15281021datastore

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.homework15281021datastore.databinding.FragmentProfileBinding
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


val Context.dataStore: DataStore<Preferences> by preferencesDataStore("Info")

class ProfileFragment : BaseFragment<FragmentProfileBinding>(FragmentProfileBinding::inflate) {
    val imgurlkey = stringPreferencesKey("imgurl")
    val firstnamekey = stringPreferencesKey("firstname")
    val lastnamekey = stringPreferencesKey("lastname")
    val agekey = stringPreferencesKey("age")
    val emailkey = stringPreferencesKey("email")
    val genderkey = stringPreferencesKey("gender")
    val addresskey = stringPreferencesKey("address")
    val phonenumberkey = stringPreferencesKey("phonenumber")


    override fun start() {

    listeners()

    }

    private fun listeners() {
        binding.read.setOnClickListener{
            viewLifecycleOwner.lifecycleScope.launch {
                withContext(IO) {
                    read()
                }
            }
        }


        binding.editprofile.setOnClickListener{
            findNavController().navigate(R.id.action_profileFragment_to_editProfileFragment2)
        }
    }

    private suspend fun read() {
        val imgurl = requireContext().dataStore.data.map {preference ->
            preference[EditProfileFragment().imgurlkey]?: ""
        }.first()

        val firstname = requireContext().dataStore.data.map {preference ->
            preference[EditProfileFragment().firstnamekey]?: ""
        }.first()

        val lastname = requireContext().dataStore.data.map {preference ->
            preference[EditProfileFragment().lastnamekey]?: ""
        }.first()

        val age = requireContext().dataStore.data.map {preference ->
            preference[EditProfileFragment().agekey]?: ""
        }.first()

        val email = requireContext().dataStore.data.map {preference ->
            preference[EditProfileFragment().emailkey]?: ""
        }.first()

        val gender = requireContext().dataStore.data.map {preference ->
            preference[EditProfileFragment().genderkey]?: ""
        }.first()

        val phonenumber = requireContext().dataStore.data.map {preference ->
            preference[EditProfileFragment().phonenumberkey]?: ""
        }.first()

        binding.firstname.setText(firstname)
        binding.background.setImage(imgurl)
        binding.lastname.setText(lastname)
        binding.age.setText(age)
        binding.email.setText(email)
        binding.phonenumber.setText(phonenumber)
        binding.gender.setText(gender)

    }


}