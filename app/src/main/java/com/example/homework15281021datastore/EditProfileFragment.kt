package com.example.homework15281021datastore


import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey

import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.homework15281021datastore.databinding.FragmentEditProfileBinding
import kotlinx.coroutines.Dispatchers.IO

import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class EditProfileFragment : BaseFragment<FragmentEditProfileBinding>(FragmentEditProfileBinding::inflate) {


    val imgurlkey = stringPreferencesKey("imgurl")
    val firstnamekey = stringPreferencesKey("firstname")
    val lastnamekey = stringPreferencesKey("lastname")
    val agekey = stringPreferencesKey("age")
    val emailkey = stringPreferencesKey("email")
    val genderkey = stringPreferencesKey("gender")
    val addresskey = stringPreferencesKey("address")
    val phonenumberkey = stringPreferencesKey("phonenumber")


    override fun start() {

        binding.save.setOnClickListener{
            viewLifecycleOwner.lifecycleScope.launch {
                withContext(IO){
                    write()
                }
            }

            findNavController().navigate(R.id.action_editProfileFragment2_to_profileFragment)
        }
    }



    private suspend fun write() {
        val imgurl = binding.imgurl.text.toString()
        val firstname = binding.firstname.text.toString()
        val lastname = binding.lastname.text.toString()
        val age = binding.age.text.toString()
        val email = binding.email.text.toString()
        val gender = binding.gender.text.toString()
        val address = binding.address.text.toString()
        val phonenumber = binding.phonenumber.text.toString()

        requireContext().dataStore.edit {
            it[imgurlkey] = imgurl
            it[firstnamekey] = firstname
            it[lastnamekey] = lastname
            it[agekey] = age
            it[emailkey] = email
            it[genderkey] = gender
            it[addresskey] = address
            it[phonenumberkey] = phonenumber
        }
    }


}